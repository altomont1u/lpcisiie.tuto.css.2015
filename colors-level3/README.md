ALTOMONTE,CHOGNARD, WITTMANN

Colors level 3

Ressource W3C : http://www.w3.org/TR/css3-color/

Définition :

Color permet de modifier la couleur d'un élément graphique. 

Les propriétés associés sont les suivantes :

- color -> change la couleur de l'élément.

	La couleur peut être changer de différente manières :

	- par mot clef : blue, black, les couleurs en général mais 	aussi : transparent, currentColor

	- par valeur RGB : (255,45,255) ou (100%,0%,100%)

	- par référence code couleur héxadécimal : #ff0000

	- par RGBA : (255,45,255,0.7) ou (100%,0%,100%,0.7) A 		représente ici l'opacité

	- par HSL  : (100%, 25%, 32%)

	- par HSLA  : (100%, 25%, 32%, 0.7) A représente ici l'opacité

- opacity -> change l'opacité d'un élément

	- Valeur comprise entre 0.0(transparent) et 1.0(opaque)

Compatibilité : firefox, chrome, IE, safari



