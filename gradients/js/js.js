  // * random color script
  $(document).ready(function(){
    $('.shuffler').each(function(){
      $(this).click(function(){
        var randomColor = Math.random().toString(16).slice(2, 8);
        var theColor = "#"+randomColor
        $(this).prev("input").val(theColor);
        $(this).css("background",theColor);
        $(this).prev(".hex").css("border-bottom-color",theColor);
      });
    });
  });
  // random color script *

  var c = document.getElementById("dessin");

  $("#set").click(function(){
    var c = document.getElementById("dessin");
    var ctx = c.getContext("2d");
    grd=ctx.createLinearGradient(300,300,0,0);
    grd.addColorStop(0,$("#color1").val());
    grd.addColorStop(1,$("#color2").val());
    ctx.fillStyle=grd;
    ctx.fillRect(0,0,640,1136);
  });
